import { createStore } from 'vuex'
import api from '../api'

export default createStore({
  namesapced: true,
  state: {
    token: null,
    isAuthenticated: false,
    role: null,
    user_id: null,
    credentials: [{ username: '', password: '' }],
  },

  getters: {},

  mutations: {},

  actions: {},

  modules: {}
})
